%% Start
% Input
% * w: The width of the picture
% * h: The height of the picture
% * format: format of the raw picture ('Q10' or 'M10');
% * pattern: sensor pattern ('bggr' or 'grbg');

function blacklevel(w,h,format,pattern)
oldFolder = uigetdir;
cd(oldFolder);

img_path_list =  dir(fullfile('*.raw')); % Find the *.raw file
img_num = length(img_path_list);         % Count how many '*.raw' files

results = cell(img_num,5);
value_to_excel = cell(img_num+1,5);

if img_num > 0                                 % If the folder has files
    for i = 1:img_num                          % Read the image
            image_name = img_path_list(i).name;% Get the image name
            image = fopen(image_name,'rb','b');
         
            if strcmpi(format, 'm10')
                 sizet = w*h*5/4 + h*4;
                 data = fread(image,sizet,'*uint8', 0,'l' );
                 data = uint16(data);
                 data = reshape(data, [w*5/4+4, h]);
        
        %Remove the 4-bytes jpadding
                 data = data(1:w*5/4, :);
        
        %Split the 5th element
                 data = reshape(data, [5, w*h/4]);
        
                 major = data(1:4, :);
                 minor = data(5, :);
                 minor1 = bitand(minor,3);
                 minor2 = bitand(bitshift(minor, -2), 3);
                 minor3 = bitand(bitshift(minor, -4), 3);
                 minor4 = bitand(bitshift(minor, -6), 3);
                 minor = [minor1;minor2;minor3;minor4];
        
                 ret = bitor(bitshift(major, 2), minor);
                 ret = reshape(ret, [w,h]);
                 retmap = ret';
                 img = uint8(bitshift(retmap, -2));
                 
             elseif strcmpi(format,'q10')
                 size= w*h/6;
                 [raw,l] = fread(image, size, 'uint64=>uint64','l');

                 image_test(1,1:size) = uint16(bitand(raw(1:size),1023));
                 image_test(2,1:size) = uint16(bitshift(bitand(raw(1:size),2^20 - 2^10),-10));
                 image_test(3,1:size) = uint16(bitshift(bitand(raw(1:size),2^30 - 2^20),-20));
                 image_test(4,1:size) = uint16(bitshift(bitand(raw(1:size),2^40 - 2^30),-30));
                 image_test(5,1:size) = uint16(bitshift(bitand(raw(1:size),2^50 - 2^40),-40));
                 image_test(6,1:size) = uint16(bitshift(bitand(raw(1:size),2^60 - 2^50),-50));
                 img1 = reshape(image_test,[w h]);
                 img2 = img1';
                 img = uint8(bitshift(img2,-(10-8)));
            end
            
            if strcmpi(pattern, 'grbg')
                Gr = img(1:2:end, 1:2:end);
                R = img(1:2:end, 2:2:end);
                B = img(2:2:end, 1:2:end);
                Gb = img(2:2:end, 2:2:end);
                
            elseif strcmpi(pattern, 'bggr')
                B = img(1:2:end, 1:2:end);
                Gb = img(1:2:end, 2:2:end);
                Gr = img(2:2:end, 1:2:end);
                R = img(2:2:end, 2:2:end);
            end
            
            Bave = mean(B(:));
            Grave = mean(Gr(:));
            Gbave = mean(Gb(:));
            Rave = mean(R(:));
            
            results_write = {image_name Bave Gbave Grave Rave};
            results(i,:) = results_write;
            
    end
end

value_to_excel(1,:) = {'ISO','B_avg','Gb_avg','Gr_avg','R_avg'};
value_to_excel(2:img_num+1,:) = results;


temp = value_to_excel(2:img_num+1,1);
temp1 = cell2mat(temp);
value_to_excel(2:img_num+1,1) = cellstr(temp1(1:img_num,1:4));

xlswrite('BlackLevel.xlsx',value_to_excel);

end